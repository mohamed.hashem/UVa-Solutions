import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.Queue;
import java.io.BufferedReader;
import java.util.LinkedList;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 *
 * @author Mohamed Hashem
 */
public class Main {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(1, in, out);
        out.close();
    }

    static class Task {
        int[][] arr;
        int[][] dp;

        public void solve(int testNumber, InputReader in, PrintWriter out) {
            int t = in.nextInt();
            arr = new int[105][105];
            dp = new int[105][105];
            while (t-- > 0) {
                String s = in.next();
                int n, m;
                n = in.nextInt();
                m = in.nextInt();
                for (int i = 0; i < n; ++i)
                    for (int j = 0; j < m; ++j)
                        arr[i][j] = in.nextInt();
                int MAX = 0;
                for (int i = 0; i < n; ++i) {
                    for (int j = 0; j < m; ++j) {
                        for (int k = 0; k < n; ++k) {
                            for (int l = 0; l < m; ++l) {
                                dp[k][l] = 0;
                            }
                        }
                        int x, y;
                        x = i;
                        y = j;

                        Queue<Pair> que = new LinkedList<>();
                        que.add(new Pair(x, y));
                        while (que.size() != 0) {
                            Pair cur = new Pair(que.peek().first, que.peek().second);
                            que.remove();
                            if (cur.first - 1 >= 0 && arr[cur.first - 1][cur.second] < arr[cur.first][cur.second]) {
                                if (dp[cur.first][cur.second] + 1 > dp[cur.first - 1][cur.second]) {
                                    dp[cur.first - 1][cur.second] = dp[cur.first][cur.second] + 1;
                                    que.add(new Pair(cur.first - 1, cur.second));
                                }
                            }
                            if (cur.first + 1 < n && arr[cur.first + 1][cur.second] < arr[cur.first][cur.second]) {
                                if (dp[cur.first][cur.second] + 1 > dp[cur.first + 1][cur.second]) {
                                    dp[cur.first + 1][cur.second] = dp[cur.first][cur.second] + 1;
                                    que.add(new Pair(cur.first + 1, cur.second));
                                }
                            }
                            if (cur.second - 1 >= 0 && arr[cur.first][cur.second - 1] < arr[cur.first][cur.second]) {
                                if (dp[cur.first][cur.second] + 1 > dp[cur.first][cur.second - 1]) {
                                    dp[cur.first][cur.second - 1] = dp[cur.first][cur.second] + 1;
                                    que.add(new Pair(cur.first, cur.second - 1));
                                }
                            }
                            if (cur.second + 1 < m && arr[cur.first][cur.second + 1] < arr[cur.first][cur.second]) {
                                if (dp[cur.first][cur.second] + 1 > dp[cur.first][cur.second + 1]) {
                                    dp[cur.first][cur.second + 1] = dp[cur.first][cur.second] + 1;
                                    que.add(new Pair(cur.first, cur.second + 1));
                                }
                            }
                        }
                        for (int k = 0; k < n; ++k) {
                            for (int l = 0; l < m; ++l) {
                                MAX = Math.max(MAX, dp[k][l]);
                            }
                        }

                    }
                }
                out.printf("%s: %d\n", s, MAX + 1);
            }

        }

        class Pair {
            int first;
            int second;

            Pair(int first, int second) {
                this.first = first;
                this.second = second;
            }

        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

    }
}

