import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 *
 * @author Mohamed Hashem
 */
public class Main {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(1, in, out);
        out.close();
    }

    static class Task {
        public void solve(int testNumber, InputReader in, PrintWriter out) {
            int[][] dp = new int[101][101];
            int t = in.nextInt();
            while (t-- > 0) {
                for (int i = 0; i < 101; ++i) {
                    for (int j = 0; j < 101; ++j) {
                        dp[i][j] = -1;
                    }
                }
                int n = in.nextInt();
                for (int i = 0; i < n; ++i) {
                    for (int j = 0; j <= i; ++j) {
                        dp[i][j] = in.nextInt();
                    }
                }
                for (int i = 1; i < n; ++i) {
                    for (int j = 0; j <= i; ++j) {
                        if (j == 0) dp[i][j] = dp[i][j] + dp[i - 1][j];
                        else {
                            dp[i][j] = Math.max(dp[i - 1][j] + dp[i][j], dp[i - 1][j - 1] + dp[i][j]);
                        }
                    }
                }
                int ans = -1;
                for (int i = 0; i < n; ++i) {
                    ans = Math.max(dp[n - 1][i], ans);
                }
                out.println(ans);
            }

        }

    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

    }
}

