#include <bits/stdc++.h>
using namespace std;
pair<int,int>arr[(int)1e3 + 6];
vector<pair<double,pair<int,int> > >vec;
int parent[(int)1e4 + 6];
int rnk[(int)1e4 + 6];
int findparent(int x){
  if(parent[x] == x)return x;
  return parent[x] = findparent(parent[x]);
}
void connect(int x , int y){
  int tx = findparent(x);
  int ty = findparent(y);
  if(rnk[tx] > rnk[ty]){
    parent[ty] = tx;
  }
  else if(rnk[tx] < rnk[ty]){
    parent[tx] = ty;
  }
  else{
    parent[tx] = ty;
    rnk[ty]++;
  }
}
bool isconnected(int x , int y){
  if(findparent(x) == findparent(y))return 1 ;
  else return 0 ;
}
int main()
{
   ios_base::sync_with_stdio(false);
   cin.tie(0);
   int t ; cin >> t;
   while(t--){
    int n , m ;
    cin >> n >> m ;
    for(int i = 0 ; i < (int)1e4 + 6 ; ++i)
        parent[i] = i;
    for(int i = 0 ; i < (int)1e4 + 6 ; ++i)
        rnk[i] = 0 ;
    for(int i = 0 ; i < m; ++i){
        int a , b ;
        cin >> a >> b ;
        arr[i].first = a ; arr[i].second = b;
    }
    for(int i = 0 ; i < m ; ++i){
        for(int j  = i+1 ; j < m; ++j){
            vec.push_back({sqrt(pow(arr[i].first - arr[j].first,2) + pow(arr[i].second-arr[j].second,2)),{i+1 , j+1}});
        }
    }
    sort(vec.begin(),vec.end());
    double val = 0;
    vector<double>h;
    for(int i = 0 ; i < vec.size() ; ++i){
        if(!isconnected(vec[i].second.first , vec[i].second.second)){
            connect(vec[i].second.first , vec[i].second.second);
            h.push_back(vec[i].first);
        }
    }
    sort(h.begin(),h.end());
    for(int i = 0 ; i < h.size()-n+1 ; ++i){
        val = max(h[i] , val);
    }
    printf("%.2lf\n" , val);
    vec.clear();

   }
    return 0;
}
