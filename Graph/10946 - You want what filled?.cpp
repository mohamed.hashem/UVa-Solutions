#include <bits/stdc++.h>
using namespace std;
char arr[70][70];
bool vis[70][70];
int n,m ;
int BFS(int x , int y , char c){
  queue<pair<int,int> > que;
  que.push(make_pair(x,y));
  set<pair<int,int> > counter;
  while(que.size()){
    pair<int,int>cur = que.front();
    counter.insert(make_pair(cur.first,cur.second));
    que.pop();
    vis[cur.first][cur.second] = 1 ;
    if(cur.first-1 >= 0 && arr[cur.first-1][cur.second] == c && vis[cur.first-1][cur.second] == 0){
        que.push(make_pair(cur.first-1 , cur.second));
    }
    if(cur.first+1 < n && arr[cur.first+1][cur.second] == c && vis[cur.first+1][cur.second] == 0){
        que.push(make_pair(cur.first+1 , cur.second));
    }
    if(cur.second-1 >= 0 && arr[cur.first][cur.second-1] == c && vis[cur.first][cur.second-1] == 0){
        que.push(make_pair(cur.first , cur.second -1 ));
    }
    if(cur.second + 1 < m && arr[cur.first][cur.second+1] == c && vis[cur.first][cur.second+1] == 0){
        que.push(make_pair(cur.first , cur.second + 1));
    }

  }
  return counter.size();
}
class data{
 public:
     char c; int m;
};
bool cmp(data a , data b){
    if(a.m != b.m){
        return a.m > b.m;
    }
    return a.c < b.c;
}
int main()
{
   ios_base::sync_with_stdio(false);
   cin.tie(0);
   int U = 1 ;
   while(cin >> n >> m && n && m){
        vector<int>all[28];
       for(int i = 0 ; i < n; ++i){
        string s ; cin >> s ;
        for(int j = 0 ; j < m; ++j){
            arr[i][j] = s[j];
        }
       }
       for(int i = 0 ; i < n; ++i){
        for (int j = 0 ; j < m; ++j){
            vis[i][j] = 0;
        }
       }

       for(int i = 0 ; i < n; ++i){
        for(int j = 0 ; j < m; ++j){
            if(arr[i][j] != '.' && vis[i][j] == 0){
                int cnt = BFS(i,j,arr[i][j]);
                all[arr[i][j]-'A'].push_back(cnt);
            }
        }
       }
       vector<data>vec;
       for(int i = 0 ; i < 28 ; ++i){
        if(all[i].size() != 0){
                for(int j = 0 ; j < all[i].size() ; ++j){
                     data l ; l.c = ((char)(i+'A')); l.m = all[i][j];
                     vec.push_back(l);
            }
        }
       }
       sort(vec.begin(),vec.end(),cmp);
        cout << "Problem "; cout << U; cout<<":"<<endl;
       for(int i = 0 ; i < vec.size() ; ++i){
        cout << vec[i].c<<" " <<vec[i].m<<endl;
       }
       U++;

   }
    return 0;
}
