#include <bits/stdc++.h>
using namespace std;
char arr[200][200];
bool vis[200][200];
int n,m ;
void BFS(int x , int y){
  queue<pair<int,int> > que;
  que.push(make_pair(x,y));
  while(que.size()){
    pair<int,int>cur = que.front();
    que.pop();
    vis[cur.first][cur.second] = 1 ;
    if(cur.first-1 >= 0 && arr[cur.first-1][cur.second] == '@' && vis[cur.first-1][cur.second] == 0){
        que.push(make_pair(cur.first-1 , cur.second));
    }
    if(cur.first+1 < n && arr[cur.first+1][cur.second] == '@' && vis[cur.first+1][cur.second] == 0){
        que.push(make_pair(cur.first+1 , cur.second));
    }
    if(cur.second-1 >= 0 && arr[cur.first][cur.second-1] == '@' && vis[cur.first][cur.second-1] == 0){
        que.push(make_pair(cur.first , cur.second -1 ));
    }
    if(cur.second + 1 < m && arr[cur.first][cur.second+1] == '@' && vis[cur.first][cur.second+1] == 0){
        que.push(make_pair(cur.first , cur.second + 1));
    }
    if(cur.first-1 >= 0 &&cur.second -1 >= 0 && arr[cur.first-1][cur.second-1] == '@' && vis[cur.first-1][cur.second-1] == 0){
        que.push(make_pair(cur.first-1 , cur.second-1));
    }
    if(cur.first+1 < n &&cur.second +1 < m && arr[cur.first+1][cur.second+1] == '@' && vis[cur.first+1][cur.second+1] == 0){
        que.push(make_pair(cur.first+1 , cur.second+1));
    }
    if(cur.first-1 >= 0 &&cur.second +1 < m && arr[cur.first-1][cur.second+1] == '@' && vis[cur.first-1][cur.second+1] == 0){
        que.push(make_pair(cur.first-1 , cur.second+1));
    }
    if(cur.first+1 < n &&cur.second -1 >= 0 && arr[cur.first+1][cur.second-1] == '@' && vis[cur.first+1][cur.second-1] == 0){
        que.push(make_pair(cur.first+1 , cur.second-1));
    }
  }
}
int main()
{
   ios_base::sync_with_stdio(false);
   cin.tie(0);
   while(cin >> n >> m && n && m){
       for(int i = 0 ; i < n; ++i){
        string s ; cin >> s ;
        for(int j = 0 ; j < m; ++j){
            arr[i][j] = s[j];
        }
       }
       for(int i = 0 ; i < n; ++i){
        for (int j = 0 ; j < m; ++j){
            vis[i][j] = 0;
        }
       }
       int ans = 0 ;
       for(int i = 0 ; i < n; ++i){
        for(int j = 0 ; j < m; ++j){
            if(arr[i][j] == '@' && vis[i][j] == 0){
                BFS(i,j);
                ans++;
            }
        }
       }
       cout << ans <<endl;

   }
    return 0;
}
