#include <bits/stdc++.h>
using namespace std;
int n , m ;
int main()
{
   ios_base::sync_with_stdio(false);
   cin.tie(0);
  while(cin >> n >> m && n && m){
    int arr[n] , a[m];
    bool vis[n];
    memset(vis,false,sizeof vis);
    for(int i = 0 ; i < n; ++i)
        cin >> arr[i];
    for(int j = 0 ; j < m; ++j)
        cin >> a[j];
    sort(arr,arr+n);
    sort(a,a+m);
    int i = 0 , j = 0 ;
    int sum = 0 ;
    while(i < n && j < m){

        if(arr[i] <= a[j]){
            vis[i] = 1 ;
            ++i;
            sum += a[j];
            ++j;
        }
        else if(arr[i] > a[j]){
            ++j;
        }
    }
    bool c = 0 ;
    for(int i = 0 ; i < n; ++i)
        if(vis[i] == 0) c = 1 ;
    if(c)cout << "Loowater is doomed!"<<endl;
    else{
        cout << sum << endl;
    }
  }
    return 0;
}
