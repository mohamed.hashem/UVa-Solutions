#include <bits/stdc++.h>
using namespace std;
int parent[(int)1e5 + 6];
int rnk[(int)1e5 + 6];
int findparent(int x){
  if(parent[x] == x)return x;
  return parent[x] = findparent(parent[x]);
}
void connect(int x , int y){
  int tx = findparent(x);
  int ty = findparent(y);
  if(rnk[tx] > rnk[ty]){
    parent[ty] = tx;
  }
  else if(rnk[tx] < rnk[ty]){
    parent[tx] = ty;
  }
  else{
    parent[tx] = ty;
    rnk[ty]++;
  }
}
bool isconnected(int x , int y){
  if(findparent(x) == findparent(y))return 1 ;
  else return 0 ;
}
int cnt[(int)1e5 + 6];
int main()
{
   ios_base::sync_with_stdio(false);
   cin.tie(0);
   int t ;
   cin >> t ;
   while(t--){
    int n ; cin >> n ;
    for(int i = 0 ; i < (int)1e5 + 6 ; ++i)
        parent[i] = i;
    for(int  i = 0 ; i < (int)1e5 + 6 ; ++i)
        rnk[i] = 0 ;
    map<string,int>mp;
    vector<pair<int,int> > vec;
    int c = 1 ;
    for(int i = 0 ; i < n; ++i){
        string a ,b ;
        cin >> a >> b ;
        if(mp[a] == 0){
            mp[a] = c;
            ++c;
        }
        if(mp[b] == 0){
            mp[b] = c;
            ++c;
        }
        vec.push_back({mp[a] , mp[b]});
    }
    for(int i = 0 ; i < (int)1e5 + 6 ; ++i)
        cnt[i] = 1 ;
    for(int i = 0 ; i < vec.size() ; ++i){
       if(isconnected(vec[i].first , vec[i].second)){
           cout << max(cnt[findparent(vec[i].first)] , cnt[findparent(vec[i].second)])<<endl;
       }
       else{
           int l = cnt[findparent(vec[i].first)];
           int r = cnt[findparent(vec[i].second)];

           connect(vec[i].first , vec[i].second);
           cnt[findparent(vec[i].first)] = (l+r);
           cout << cnt[findparent(vec[i].first)]<<endl;
       }
    }
   }

    return 0;
}
