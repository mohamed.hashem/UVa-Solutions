#include <bits/stdc++.h>
using namespace std;
int parent[(int)1e5 + 6];
int rnk[(int)1e5 + 6];
int findparent(int x){
  if(parent[x] == x)return x;
  return parent[x] = findparent(parent[x]);
}
void connect(int x , int y){
  int tx = findparent(x);
  int ty = findparent(y);
  if(rnk[tx] > rnk[ty]){
    parent[ty] = tx;
  }
  else if(rnk[tx] < rnk[ty]){
    parent[tx] = ty;
  }
  else{
    parent[tx] = ty;
    rnk[ty]++;
  }
}
bool isconnected(int x , int y){
  if(findparent(x) == findparent(y))return 1 ;
  else return 0 ;
}
int cnt[(int)1e5 + 6];
int main()
{
   ios_base::sync_with_stdio(false);
   cin.tie(0);
   int t;
   cin >> t ;
   while(t--){
    int n , m ; cin >> n >>  m;
    for(int i = 0 ; i < (int)1e5 + 6 ; ++i)
        parent[i] = i;
    for(int i = 0 ; i < (int)1e5 + 6 ; ++i)
        rnk[i] = 0 ;
    for(int i = 0 ; i < (int)1e5 + 6 ; ++i)
        cnt[i] = 1 ;
    for(int i = 0 ; i < m; ++i){
        int a , b ;
        cin >> a >> b ;
        if(!isconnected(a,b)){
            int l = cnt[findparent(a)];
            int r = cnt[findparent(b)];
            connect(a,b);
            cnt[findparent(a)] = l +r;
        }
    }
    int ans = 1 ;
    for(int i = 1 ; i <= n ; ++i)
        ans = max(ans,cnt[i]);
    cout << ans <<endl;
   }

    return 0;
}
